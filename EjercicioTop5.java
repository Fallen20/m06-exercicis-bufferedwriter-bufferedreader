package top5;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class EjercicioTop5 {
//	static Path archivo=Paths.get("C:\\Users\\monts\\Desktop\\a\\a.txt");//casa
	static Path archivo=Paths.get("/home/users/inf/wiam2/iam47264842/Escritorio/carpeta/a.txt");
	public static void main(String[] args) throws IOException {
		comprobacion("Toby Fox",250);
		
	}
	
	public static void comprobacion(String nombre, int numero) throws IOException {
		//mira cuantas lineas hay>read
		List<String> lineas=Files.lines(archivo).collect(Collectors.toList());//esto guarda TODAS las lineas en un list y puedes hacer un bucle y todo
		//System.out.print(lineas);
		
		if(lineas.size()<5) {escribir1Linea(nombre, numero);}//son 6 porque tiene titulo. CAMBIAR
		else if(lineas.size()>=5) {juntarSplit(nombre, numero);}
		
		//+5>mira los numeros
			//comprueba si el numero nuevo es menor al ultimo>si es mas pequeño no haces nada
			//si es mayor> llama al metodo BorrarYEscribir
		//si hay -5 directamente escribe>metodo escribir
	}
	
	
	public static void escribir1Linea(String nombre, int numero) throws IOException {
		BufferedWriter escribe=Files.newBufferedWriter(archivo, StandardOpenOption.APPEND);//append hace que anada otra linea y no borra lod de antes
		escribe.write(nombre+":"+numero);//lo convierte todo en string
		escribe.newLine();//separa scores
		escribe.close();
		//escribe solo
	}
	
	public static String[] splitear(String linea) {
		return linea.split(":");
	}
	
	public static void juntarSplit(String nombre, int numero) throws IOException{
		List<String> lineas=Files.lines(archivo).collect(Collectors.toList());//esto guarda TODAS las lineas en un list y puedes hacer un bucle y todo
		
		String[] partes1 = splitear(lineas.get(0));
		String[] partes2 = splitear(lineas.get(1));
		String[] partes3 = splitear(lineas.get(2));
		String[] partes4 = splitear(lineas.get(3));
		String[] partes5 = splitear(lineas.get(4));
		
		String numString=Integer.toString(numero);
		
		//si el numero es mas grande que partes[1], nada
		//si el numero es mas peque que partes[1]
		String[] puntuaciones=new String[] {partes1[1],partes2[1],partes3[1],partes4[1],partes5[1],numString};
			
		asignar(puntuaciones, nombre, numero);
		//hacer un split con el ultimo argumento y hacer un sorted, el que compara (sobrescribir)
		//con las cosas ordenadas volverlo a escribir
	}
	
	public static String[] bubbleSort(String arr[]){
		
        int longitud = arr.length;
        for (int i = 0; i < longitud-1; i++) {
            for (int j = 0; j < longitud-i-1; j++) {
                if (Integer.parseInt(arr[j]) <= Integer.parseInt(arr[j+1])){
                    String temp =  arr[j];
                    
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        return arr;
	}
	
	
	
	public static void asignar(String[] puntuaciones, String nombre, int numero) throws IOException {		
		
		//para comprobar que esa persona ya se ha puesto, por si hay dos personas con la misma puntuacion que no se ponga la misma en ambas
		List<String> lineas=Files.lines(archivo).collect(Collectors.toList());//lista origian

		String[][] arrayConNombres=new String[2][lineas.size()];//donde se guardan el nombre y la puntuacion ordenadas
		//2 plazas [nombre, puntuacion], pero solo 5 lineas
		String[] ordenado=bubbleSort(puntuaciones);//ordenada

		
		for(int contador=0;contador<lineas.size();contador++) {
			
			String[] partes = splitear(lineas.get(contador));
			
			if (Integer.parseInt(ordenado[contador])==Integer.parseInt(partes[1])) {
				arrayConNombres[0][contador]=partes[0];
				arrayConNombres[1][contador]=partes[1];
			
			}
			if (Integer.parseInt(ordenado[contador])==numero) {
				arrayConNombres[0][contador]=nombre;
				arrayConNombres[1][contador]=Integer.toString(numero);
			
			}
		}
		escribirFinal(arrayConNombres);
		
	}
	
	public static void escribirFinal(String[][] arrayConNombres) throws IOException {
		BufferedWriter escribe=Files.newBufferedWriter(archivo);
		BufferedWriter escribeAppend=Files.newBufferedWriter(archivo, StandardOpenOption.APPEND);
		
		for(int contador=0;contador>=5;contador++) {
			for(int contador2=0;;contador2++) {
				if(contador2>5) {break;}
				
				if (contador2==0) {
					escribe.write(arrayConNombres[0][contador2]+":"+arrayConNombres[1][contador2]);//lo convierte todo en string
					escribe.newLine();//separa scores.
				}
				else {
					escribeAppend.write(arrayConNombres[0][contador2]+":"+arrayConNombres[1][contador2]);//lo convierte todo en string
					escribeAppend.newLine();//separa scores.
					
				}
			}
			
			
		}
		escribe.close();
		
	}
	//escribe solo hasta el 5
	/*Bob:300
Maria:300
Pepe:300
Puppet:300
AAA:100*/
}
